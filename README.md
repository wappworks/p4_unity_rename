# Perforce Unity File Renamer
-----

## Overview
This tool (p4_unity_rename) is an utility to simplify renaming Unity files in Perforce.

## Installation
To install the tool as the package `p4-unity-rename`:

```
pip install git+https://bitbucket.org/wappworks/p4_unity_rename.git#egg=p4-unity-rename
```

## Usage
Run `p4_unity_rename <src filename> <dest_filename>` to rename files

## License
This tool is released under the [MIT license](LICENSE.md)