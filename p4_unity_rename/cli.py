import subprocess
import click


@click.command()
@click.argument('src')
@click.argument('dest')
def cmd_rename(src, dest):
    click.echo("Renaming {} to {}".format(src, dest))

    src_meta = src + ".meta"
    dest_meta = dest + ".meta"

    if not src or not dest:
        click.echo("A source file and a destination filename must be specified", err=True)
        raise click.Abort()

    subprocess.call(["p4", "edit", src])
    subprocess.call(["p4", "edit", src_meta])
    subprocess.call(["p4", "move", src, dest])
    subprocess.call(["p4", "move", src_meta, dest_meta])
